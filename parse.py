# -*- coding: utf-8 -*-
"""
Запускать на python3

apt-get install python3-venv
python3 -m venv myenv
source myenv/bin/activate
python -m parser --help

для ручного управления
python
from models import *



В планах на доработку:
  * Добавить организации (помимо филиалов, низкоприоритетно, ибо не нужно)
  * Добавить регионы
    В каждом горде есть несколько регионов - например, центральный, калилинский. Также идёт разбитие по более
    мелким городам, например Тобольск объединён с Тюменью
  * Добавить релизы
    Суть такова - при первом парсинге сливать всё в initial release
    При обновлении компаний в дальнейшем необновлённые компании останутся со старым номером релиза и их можно будет
    удалить
    На данный момент нельзя точно сказать какие компании есть в 2gis, а каких нет
"""

from __future__ import unicode_literals, absolute_import

import argparse
import logging
import time
import random
import logging.handlers

from models import Region, Company, Email, Phone, Rubric, CompanyRubric, Migration


logger = logging.getLogger('2gis')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

file_handler = logging.handlers.RotatingFileHandler(
    '2gis.log', mode='a', maxBytes=15 * 1024 * 1024, backupCount=2, encoding=None, delay=0)
file_handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
file_handler.setFormatter(formatter)

logger.addHandler(ch)
logger.addHandler(file_handler)


parser = argparse.ArgumentParser(
    description='Парсер 2gis.',
    formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument(
    'action',
    choices=[
        'migrate',
        'parse_rubrics',
        'parse_companies',
        'show_regions',
        'parse_photos',
        'dump_all_phones',
        'dump_all_emails',
    ],
    help='Возможные действия:\n'
         'migrate - миграции бд\n'
         'parse_rubrics - загрузить только рубрики по указанным городам\n'
         'dump_all_emails - дамп email\n'
         'dump_all_phones - дамп телефотов\n'
         'show_regions - показать все регионы в базе\n'
         'parse_photos - парсим фотки у компаний в базе\n'
         'parse_companies - парсинг городов\n\n'
         'Примеры:\n'
         'Создаём бд и заполняем её городами: python -m <prog> syncdb; python -m <prog> init\n'
         'Парсим всё в Тюмени: python -m <prog> parse_companies --regions 13')
parser.add_argument(
    '--regions',
    dest='regions',
    type=str,
    nargs='+',
    help='Использовать только перечисленные id городов')
parser.add_argument(
    '--exclude-regions',
    dest='exclude_regions',
    type=str,
    nargs='+',
    help='Исключить id городов из выборки')
parser.add_argument(
    '-f', '--force',
    action='store',
    default=False,
    const=True,
    dest='force',
    nargs='?',
    help='Выключить проверки на количество компаний в базе, принудительно обновлять')
parser.add_argument(
    '--dont-fetch-rubrics',
    action='store',
    default=True,
    const=False,
    dest='fetch_rubrics',
    nargs='?',
    help='Не обновлять список рубрик при получении компаний в городе. Полезно, когда парсинг идёт не первый раз')
parser.add_argument(
    '--rubrics',
    dest='rubrics',
    type=str,
    nargs='+',
    help='Использовать только перечисленные рубрики (точное название, напр. '
         '--rubrics \'Натяжные потолки\' \'Перетяжка салона\')')
parser.add_argument(
    '--from-rubric-index',
    dest='from_rubric_index',
    action='store',
    type=int,
    help='Пропускает рубрики до данного индекса. Бывает удобно, когда парсер завалился где-то на середине. '
         'Внимание! По всем переданным городам!'
)
parser.add_argument(
    '--outfile',
    nargs='?',
    dest='outfile',
    help='Файл для вывода. По умолчанию 2gis_emails.txt'
)
parser.add_argument(
    '--fake',
    action='store',
    default=False,
    const=True,
    dest='fake',
    nargs='?',
    help='На данный момент работает только в миграциях - фейково их создаёт')


def supervisor(function, *args, **kwargs):
    """
    Позволяет перезапустить какой-то участок кода несколько раз до его успешного выполнения
    """
    def run():
        return function(*args, **kwargs)

    counter = 0

    while True:
        try:
            return run()
        except Exception as e:
            counter += 1

            if counter > 50:
                logger.error('Выполнение прервано.')
                logger.error(e, exc_info=True)
                raise

            wait = random.randrange(2, 7)

            logger.error('Выполнение прервано ({}). Ждём {} секунд(ы). Ошибка:'.format(counter, wait), exc_info=True)
            time.sleep(wait)


def args_range(old_list):
    """
    Позволяет изменять параметры [1 2 9-12] в [1 2 9 10 11 12]
    """
    new_list = []

    for r in old_list:
        if '-' in r:
            items = [int(item) for item in r.split('-')]
            new_list.extend(list(range(items[0], items[1] + 1)))
        else:
            new_list.append(int(r))

    return new_list


def get_region_qs(regions=None, exclude_regions=None):
    qs = Region.select()

    if exclude_regions:
        qs = qs.where(Region.id.not_in(exclude_regions))

    if regions:
        qs = qs.where(Region.id.in_(regions))

    return qs


def dump(cls, filename):
    if args.regions:
        # жрёт много памяти, надо что-то с этим делать
        regions = get_region_qs(args.regions, args.exclude_regions)

        qs = cls.select().join(Company).join(
            CompanyRubric, on=(CompanyRubric.company == cls.company)
        ).where(Company.region.in_(regions))
    else:
        qs = cls.select()

    with open(filename, 'w') as outfile:
        for obj in qs:
            outfile.write(obj.value + '\r\n')
    logger.info('Готово.')


def parse_companies():
    # сливаем города
    regions = list(get_region_qs(args.regions, args.exclude_regions).order_by(Region.id.asc()))

    for region in regions:
        if args.fetch_rubrics:
            region.parse_all_rubrics()
        logger.info('Парсим регион {}/{}'.format(regions.index(region) + 1, len(regions)))

        if args.rubrics:
            rubrics = list(filter(lambda x: bool(x), [Rubric.get_by_name(name, region) for name in args.rubrics]))
        else:
            rubrics = list(region.rubrics.select().where(~(Rubric.parent >> None)).order_by(Rubric.id.asc()))

        for rubric in rubrics:
            if args.from_rubric_index and rubrics.index(rubric) < args.from_rubric_index:
                logger.debug('skip {}'.format(rubric.id))
                continue

            logger.info(
                'Рубрика {r_ind}/{r_tot} ("{r_name}", '
                'регион "{reg_name}" ({reg_id}) {reg_ind}/{reg_tot})'.format(
                    r_ind=rubrics.index(rubric) + 1,
                    r_tot=len(rubrics),
                    r_name=rubric.name,
                    reg_name=region.name,
                    reg_ind=regions.index(region) + 1,
                    reg_tot=len(regions),
                    reg_id=region.id))
            supervisor(rubric.parse_companies, force=args.force)


def parse_photos():
    def get_total_in_region(rubrics):
        return Company.select().join(CompanyRubric).where(CompanyRubric.rubric.in_(rubrics)).count()

    regions = list(get_region_qs(args.regions, args.exclude_regions).order_by(Region.id.asc()))

    for region in regions:
        if args.rubrics:
            rubrics = list(filter(lambda x: bool(x), [Rubric.get_by_name(name, region) for name in args.rubrics]))
        else:
            rubrics = list(region.rubrics.select().where(~(Rubric.parent >> None)).order_by(Rubric.id.asc()))

        total_in_region = supervisor(get_total_in_region, rubrics)
        done_in_region = 0

        for rubric in rubrics:
            logger.info(
                'Рубрика {r_ind}/{r_tot} ("{r_name}", '
                'регион "{reg_name}" ({reg_id}) {reg_ind}/{reg_tot})'.format(
                    r_ind=rubrics.index(rubric) + 1,
                    r_tot=len(rubrics),
                    r_name=rubric.name,
                    reg_name=region.name,
                    reg_ind=regions.index(region) + 1,
                    reg_tot=len(regions),
                    reg_id=region.id))
            companies = list(Company.select().join(CompanyRubric).where(
                CompanyRubric.rubric == rubric
            ).order_by(Company.id.asc()))

            for company in companies:
                done_in_region += 1

                if not args.force and company.photos.exists():
                    logger.info('Пропускаю компанию {}'.format(company.name))
                    continue

                if company.has_photo_album():
                    logger.info(
                        'Комп. {c_ind}/{c_total} ({done_in_region:,} / {total_in_region:,}); '
                        'рубрика {r_ind:,} / {r_tot:,} {rub.name}; регион {r.name} - {r.id}; '
                        'компания {c_name}'.format(
                            total_in_region=total_in_region,
                            done_in_region=done_in_region,
                            r_ind=rubrics.index(rubric) + 1,
                            r_tot=len(rubrics),
                            rub=rubric,
                            c_name=company.name,
                            c_ind=companies.index(company) + 1,
                            c_total=len(companies),
                            r=region))
                    supervisor(company.parse_all_photos)


if __name__ == '__main__':
    args = parser.parse_args()
    if args.regions:
        args.regions = args_range(args.regions)
    if args.exclude_regions:
        args.exclude_regions = args_range(args.exclude_regions)

    if args.action == 'migrate':
        # создание базы
        Migration.migrate(fake=args.fake)
    elif args.action == 'parse_rubrics':
        # сливаем рубрики по городу
        qs = get_region_qs(args.regions, args.exclude_regions)
        for region in qs:
            supervisor(region.parse_all_rubrics)
        logger.info('Done.')
    elif args.action == 'parse_companies':
        supervisor(parse_companies)
    elif args.action == 'dump_all_emails':
        # дамп в файл
        dump(Email, args.outfile or '2gis_emails.txt')
    elif args.action == 'dump_all_phones':
        # дамп телефонов
        dump(Phone, args.outfile or '2gis_phones.txt')
    elif args.action == 'show_regions':
        for region in Region.select().order_by(Region.id.asc()):
            print(region.id, region.name)
    elif args.action == 'parse_photos':
        supervisor(parse_photos)

    # data = []
    # for company in Company.select():
    #     data.append({
    #         'name': company.name,
    #         'phones': [x.value for x in company.phones],
    #         'emails': [x.value for x in company.emails],
    #         'address_name': company.address,
    #     })
    #     with open('2gis.json', 'w') as outfile:
    #         json.dump(data, outfile, ensure_ascii=False, indent=4)

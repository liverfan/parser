# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import datetime
import requests
import math
import json
import time
import sys
import os

from urllib.parse import urlparse

from peewee import Model, CharField, ForeignKeyField, DateTimeField, TextField, IntegerField
from playhouse.sqlite_ext import SqliteExtDatabase


PHOTOS_DIR = 'photos'
PAGE_SIZE = 50  # 1..50

"""
    На момент написания парсера все ссылки рабочие. Их можно слить из публичного апи

    1. Заходим на главную, смотрим рубрики, выбираем рубрику (и подрубрику) чтобы отобразились организации
    2. Смотрим запросы


    key почему-то всегда одинаковый, возможно недопилили т.к. если его не указывать, то апи валятся
"""

RUBRIC_URL = (
    'https://catalog.api.2gis.ru/2.0/catalog/rubric/list?parent_id={parent}&region_id={region}'
    '&sort=popularity&fields=items.rubrics&key=ruewin2963'
)
COMPANIES_URL = (
    'https://catalog.api.2gis.ru/2.0/catalog/branch/list?page={page}&page_size={page_size}'
    '&rubric_id={rubric_id}{hash_full}&stat%5Bpr%5D=3&region_id={region_id}&fields=items.region_id'
    '%2Citems.adm_div%2Citems.contact_groups%2Citems.flags%2Citems.address%2Citems.rubrics%2Citems.name_ex%2C'
    'items.point%2Citems.external_content%2Citems.schedule%2Citems.org%2Citems.ads.options%2Citems.reg_bc_url%2C'
    'request_type%2Cwidgets%2Cfilters%2Citems.reviews%2Ccontext_rubrics%2Chash%2Csearch_attributes&key=ruewin2963'
)
COMPANY_URL = (
    'https://catalog.api.2gis.ru/2.0/catalog/branch/get?id={raw_id}'
    '&see_also_size=4&stat[pr]=1&fields=items.adm_div%2Citems.region_id%2Citems.reviews%2Citems.point%2C'
    'items.links%2Citems.name_ex%2Citems.org%2Citems.group%2Citems.see_also%2Citems.dates%2Citems.external_content'
    '%2Citems.flags%2Citems.ads.options%2Citems.email_for_sending.allowed%2Chash%2Csearch_attributes&key=ruewin2963'
)
PHOTOS_URL = (
    'https://api.photo.2gis.com/2.0/photo/get?status=active&key=gYu1s9N1wP&object_id={object_id}&'
    'object_type={object_type}&preview_size=100x100%2C304x190%2C304x%2C156x156%2C472x156%2C946x156%2C'
    '235x156%2C311x156%2C624x156%2C360x120&album_code={album_code}&locale=ru_RU'
)


def _get_url_data(url, max_tries=5, **kwargs):
    for n in range(max_tries):
        try:
            kwargs.setdefault('timeout', 20)
            return requests.get(url, **kwargs)
        except requests.exceptions.RequestException:
            if n == max_tries - 1:
                logger.critical('Something wrong with internet connection, aborting')
                raise
            logger.warn('Can not get url, waiting for some time..')
            time.sleep(10)


logger = logging.getLogger('2gis')
db = SqliteExtDatabase('my_database.db', threadlocals=True)


class classproperty(object):
    def __init__(self, f):
        self.f = f

    def __get__(self, obj, owner):
        return self.f(owner)


class BaseModel(Model):
    class Meta:
        database = db

    @classproperty
    def objects(cls):
        return cls.select()


def update_progress(progress):
    p = int(progress / 2)
    sys.stdout.write('\r[{0}] {1:.1f}%'.format('#' * p + ' ' * (50 - p), progress))
    sys.stdout.flush()


class Migration(BaseModel):
    database_version = IntegerField(unique=True)
    created_date = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def current(cls):
        try:
            return cls.select().order_by(Migration.database_version.desc()).get().database_version
        except cls.DoesNotExist:
            return 0

    @classmethod
    def migrate(cls, fake=False):
        from migrations import VERSIONS
        db.connect()

        current_version = cls.current()
        latest_version = max(VERSIONS.keys())
        if current_version < latest_version:
            for x in range(current_version + 1, latest_version + 1):
                logger.info('Проводится миграция #{}'.format(x))
                if not fake:
                    VERSIONS.get(x)(db)
                cls.create(database_version=x)
            logger.info('Миграции успешно выполнены. Текущая версия {}'.format(current_version))
        else:
            logger.info('Мигрировать нечего, версия бд последняя ({})'.format(current_version))


class Region(BaseModel):
    name = CharField(unique=True)

    def parse_all_rubrics(self):
        logger.info('Start parsing rubrics; region {} ({})'.format(self.name, self.id))
        response = _get_url_data(RUBRIC_URL.format(region=self.id, parent=0), timeout=15)
        for rubric in response.json()['result']['items']:
            local_rubric = Rubric.get_or_create(
                id=rubric['id'],
                parent=None,
                region=self,
                defaults={
                    'name': rubric['name'],
                }
            )[0]
            for subrubric in rubric['rubrics']:
                Rubric.get_or_create(
                    id=subrubric['id'],
                    parent=local_rubric,
                    region=self,
                    defaults={
                        'name': subrubric['name'],
                    }
                )


class Rubric(BaseModel):
    name = CharField()
    parent = ForeignKeyField('self', related_name='children', null=True)
    region = ForeignKeyField(Region, related_name='rubrics')
    companies_fetched_date = DateTimeField(null=True)

    def get_companies_count(self):
        return Company.select().join(CompanyRubric).where(
            Company.region == self.region,
            CompanyRubric.rubric == self
        ).count()

    @classmethod
    def get_by_name(cls, name, region=None):
        qs = cls.select().where(Rubric.name == name)
        if region:
            qs = qs.where(Rubric.region == region)

        return qs.first()

    def _get_companies(self, page=1, page_size=PAGE_SIZE, hash_full=''):
        formatted_url = COMPANIES_URL.format(
            page=page,
            page_size=page_size,
            rubric_id=self.id,
            region_id=self.region_id,
            hash_full=hash_full
        )
        logger.debug('get: {}'.format(formatted_url))
        return _get_url_data(formatted_url)

    def parse_companies(self, force=True):
        """
        Идёт два типа запросов - первый и последующие

        Первый берёт некоторые параметры из запроса (напр. хэш), кол-во страниц,
        последующие выполняются с этими параметрами
        """
        logger.info('Запрашиваем первую страницу..')
        response = self._get_companies()

        if not response.ok or 'result' not in response.json():
            logger.critical('Something gone wrong. response.ok: {}, result: {}'.format(
                response.ok,
                'result' in response.json(),
            ))
            return

        # берём нужные нам параметры
        raw_data = response.json()['result']['items']
        first_page_data = response.json()
        # скорее всего это нужно для правильной пагинации
        hash_param = '&hash={}'.format(first_page_data['result']['hash'])
        total_pages = math.ceil(first_page_data['result']['total'] / float(PAGE_SIZE))

        remote_total = first_page_data['result']['total']

        if not force:
            # проверяем по количеству нужно ли нам обновлять наши компании
            local_total = self.get_companies_count()

            logger.info('Сверяем количество в базе и на сайте по рубрике "{}"; в базе {}, на сайте {}'.format(
                self.name, local_total, remote_total
            ))

            if local_total >= remote_total:  # >= потому что могут быть старые данные. В идеале проверять по релизам
                logger.info('Количество компаний в базе и на сайте одинаково, пропускаем. Рубрика {} ({})'.format(
                    self.name, self.id))
                return

        # сливаем остальные страницы

        for page in range(2, int(total_pages) + 1):
            logger.info('стр. {}/{}'.format(page, total_pages))
            response = self._get_companies(page=page, hash_full=hash_param)
            if response.ok:
                logger.debug('Ok')
                for item in response.json()['result']['items']:
                    raw_data.append(item)
            else:
                logger.error('Not ok')
            logger.debug('-----------')

        logger.info('Обработка рубрики "{}"..'.format(self.name))

        total_companies = len(raw_data)
        total_created, total_updated = 0, 0

        for i, company in enumerate(raw_data):
            update_progress((i + 1) / total_companies * 100)

            emails, phones, sites = [], [], []

            try:
                # пытаемся найти компанию, если она у нас уже есть
                comp = Company.select().where(
                    Company.remote_id == company['id'].split('_')[0],
                    Company.org_id == company['org']['id'],
                    Company.region == self.region,
                ).get()

                # теперь обновим в ней данные, раз она уже есть
                comp.name = company['name']
                comp.address = company.get('address_name')
                comp.raw_data = company

                if comp.save():
                    total_updated += 1
            except Company.DoesNotExist:
                comp = Company.create(
                    org_id=company['org']['id'],
                    region=self.region,
                    remote_id=company['id'].split('_')[0],
                    raw_id=company['id'],
                    raw_data=json.dumps(company),
                    address=company.get('address_name'),
                    name=company['name'],
                )
                total_created += 1

            for group in company.get('contact_groups', []):
                for contact in group['contacts']:
                    if contact.get('type') == 'email':
                        if not comp.emails.where(Email.value == contact['value']).exists():
                            emails.append({
                                'value': contact['value'],
                                'company': comp
                            })
                    elif contact.get('type') == 'phone':
                        if not comp.phones.where(Phone.value == contact['value']).exists():
                            phones.append({
                                'value': contact['value'],
                                'company': comp
                            })
                    elif contact.get('type') == 'website':
                        if not comp.sites.where(Site.value == contact['text']).exists():
                            sites.append({
                                'value': contact['text'],
                                'company': comp
                            })

            with db.atomic():
                if emails:
                    Email.insert_many(emails).execute()
                if phones:
                    Phone.insert_many(phones).execute()
                if sites:
                    Site.insert_many(sites).execute()

            crubrics = []
            for rubric in company['rubrics']:
                rubric = Rubric.select().where(Rubric.id == rubric['id']).first()
                if rubric:
                    if not comp.companyrubric_set.select().where(CompanyRubric.rubric == rubric).exists():
                        crubrics.append({
                            'rubric': rubric,
                            'company': comp
                        })

            if crubrics:
                with db.atomic():
                    CompanyRubric.insert_many(crubrics).execute()

        print()
        self.companies_fetched_date = datetime.datetime.now()
        self.save()

        local_total = self.get_companies_count()
        logger.info('Рубрика "{}" обработана успешно. В базе {}, на сайте {}. Создано {}, обновлено данных {}'.format(
            self.name, local_total, remote_total, total_created, total_updated
        ))
        if local_total != remote_total:
            logger.warn('Количество в базе и на сайте не сошлось!')


class Company(BaseModel):
    """
    Модель компании

    Вообще в структуре 2gis есть организация, а есть филиалы. Между городами id организаций разные.
    Данная модель фактически является моделью филиалов. Для получения всех филиалов компании использовать
    группировку по org_id

    Уникальный id филиала можно получить из первой части поля id (до подчёркивания).
    Вторая часть - это закодированная дата (меняется незначительно с течением времени, имеет 4 похожих сегмента)

    id компании можно получить из ['org']['id']
    """
    remote_id = CharField(index=True, default='')  # todo без default
    raw_id = CharField()  # удалить
    org_id = CharField(default='', index=True)
    name = CharField()
    region = ForeignKeyField(Region, related_name='companies')
    created_date = DateTimeField(default=datetime.datetime.now)
    address = TextField(null=True)
    raw_data = TextField(default='')

    @property
    def raw_json(self):
        try:
            return json.loads(self.raw_data)
        except TypeError:
            return self.raw_data
        except json.decoder.JSONDecodeError as e:
            logger.error(e, exc_info=True)
            return {}

    @property
    def photo_albums(self):
        data = self.raw_json
        if data and 'external_content' in data:
            return [x for x in data['external_content'] if x.get('type') == 'photo_album' and x.get('count') > 0]

    def refresh_data(self, save=False):
        url = COMPANY_URL.format(raw_id=self.raw_id)
        logger.debug('get: {}'.format(url))
        raw_data = _get_url_data(url).json()
        data = raw_data['result']['items'][0]
        logger.info('Обработка компании "{}"..'.format(self.name))

        self.name = data['name']
        self.address = data.get('address_name')
        self.raw_data = json.dumps(data)

        self.org_id = data['org']['id']
        self.remote_id = data['id'].split('_')[0]
        self.raw_id = data['id']

        if save:
            self.save()

        for group in data.get('contact_groups', []):
            for contact in group['contacts']:
                if contact.get('type') == 'email':
                    Email.get_or_create(value=contact['value'], company=self)
                elif contact.get('type') == 'phone':
                    Phone.get_or_create(value=contact['value'], company=self)
                elif contact.get('type') == 'website':
                    Site.get_or_create(value=contact['text'], company=self)

    def has_photo_album(self):
        return bool(self.photo_albums)

    def _get_photos_params(self):
        photo_albums = self.photo_albums

        data = []

        for photo_album in photo_albums:
            if photo_album['count'] == 1:
                data.extend([{
                    'url': photo_album['main_photo_url'],
                    'id': photo_album['main_photo_url'].split('/')[-2],
                    'name': photo_album['main_photo_url'].split('/')[-2],
                    'type': photo_album['subtype'],
                }])

            elif photo_album['count'] > 1:
                url = PHOTOS_URL.format(
                    object_id=photo_album['main_photo_url'].split('/')[6],
                    album_code=photo_album['subtype'],
                    object_type=photo_album['main_photo_url'].split('/')[4],
                )

                photos_data = _get_url_data(url).json()

                data.extend([
                    {
                        'id': photo['id'],
                        'url': photo['url'],
                        'name': photo['url'].split('/')[-1],
                        'description': photo.get('description', ''),
                        'type': photos_data['result'][0]['album_code'],
                    }
                    for photo in photos_data['result'][0]['items']])

        return data

    def parse_all_photos(self):
        for photo in self._get_photos_params():
            if self.photos.where(Photo.remote_id == photo['id']).exists():
                continue

            path = self._get_photo(photo['url'], photo['name'])
            Photo.create(
                remote_id=photo['id'],
                description=photo.get('description', ''),
                company=self,
                name=photo['name'],
                path=path,
                type=photo['type']
            )

    def _get_photo(self, url, name):
        path = os.path.join(PHOTOS_DIR, str(self.region_id), str(self.remote_id))
        os.makedirs(path, exist_ok=True)

        logger.debug('get: {}'.format(url))
        r = _get_url_data(url, stream=True)
        if r.status_code == 200:
            with open(os.path.join(path, name), 'wb') as f:
                f.write(r.content)

        logger.info('Сохранено фото {}'.format(name))
        return path


class CompanyRubric(BaseModel):
    rubric = ForeignKeyField(Rubric)
    company = ForeignKeyField(Company)


class Email(BaseModel):
    company = ForeignKeyField(Company, related_name='emails')
    value = CharField()
    type = IntegerField(choices=[
        (1, 'from 2gis'),
        (2, 'from other'),
    ], default=1)


class Phone(BaseModel):
    company = ForeignKeyField(Company, related_name='phones')
    value = CharField()


class Site(BaseModel):
    company = ForeignKeyField(Company, related_name='sites')
    value = CharField()

    def get_url(self):
        path = self.value if '://' in self.value else 'http://' + self.value
        return urlparse(path).geturl()

    def get_html(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0'
        }
        return _get_url_data(self.get_url(), headers=headers)


class Photo(BaseModel):
    company = ForeignKeyField(Company, related_name='photos')
    remote_id = IntegerField(index=True)
    name = CharField()  # имя файла на сервере
    path = CharField()  # локальный путь
    description = TextField(default='')
    type = CharField(index=True)  # geo, common, etc.

    def delete_instance(self, *args, **kwargs):
        if os.path.exists(self.path):
            os.unlink(self.path)
        super(Photo, self).delete_instance(*args, **kwargs)


# migration
"""
from playhouse.migrate import *
from models import *

migrator = SqliteMigrator(db)

status_field = IntegerField(null=True)

migrate(
    migrator.add_column('some_table', 'status_field', status_field),
    # migrator.drop_column('some_table', 'old_column'),
)

"""
